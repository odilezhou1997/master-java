package org.tw.battle.domain.commands;

import org.tw.battle.domain.CommandHandler;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.repositories.CharacterRepository;

public class UpdateCharacterInformationCommandHandler implements CommandHandler {
    private static final String CMD_NAME = "rename-character";
    private final CharacterRepository characterRepository;

    public UpdateCharacterInformationCommandHandler(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public boolean canHandle(String commandName) {
        return CMD_NAME.equals(commandName);
    }

    @Override
    public CommandResponse handle(String[] commandArgs) throws Exception {

        if (commandArgs.length != 2
                || !commandArgs[1].matches("^[a-zA-Z-]*$")
                || commandArgs[1].length() < 3
                || commandArgs[1].length() > 64){
            return CommandResponse.fail(String.format("Bad command: %s <character id> <new name>", CMD_NAME));
        }

        try {
            Integer.parseInt(commandArgs[0]);
        } catch (Exception error) {
            return CommandResponse.fail(String.format("Bad command: %s <character id> <new name>", CMD_NAME));
        }

        String updateMessage = characterRepository.updateInformation(Integer.parseInt(commandArgs[0]), commandArgs[1]);
        return updateMessage.equals("Success")
                ? CommandResponse.success("Bad command: " + updateMessage)
                : CommandResponse.fail("Bad command: " + updateMessage);
    }

}