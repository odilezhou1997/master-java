package org.tw.battle.domain.commands;

import org.tw.battle.domain.CommandHandler;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.repositories.CharacterRepository;

/**
 * @author Liu Xia
 */
public class GetCharacterInformationCommandHandler implements CommandHandler {
    // TODO: Please implement the command handler.
    private static final String CMD_NAME = "character-info";
    private final CharacterRepository characterRepository;

    public GetCharacterInformationCommandHandler(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }
    @Override
    public boolean canHandle(String commandName) {
        return CMD_NAME.equals(commandName);
    }

    @Override
    public CommandResponse handle(String[] commandArgs) throws Exception {

        if(commandArgs.length != 1) {
            return CommandResponse.fail("Bad command: "+ CMD_NAME + " <character id>");
        }

        int id = Integer.parseInt(commandArgs[0]);
        final String message = characterRepository.characterInformation(id);
        return message.equals("character not exist")
                ? CommandResponse.fail("Bad command: " + message)
                : CommandResponse.success(message);
    }
}
