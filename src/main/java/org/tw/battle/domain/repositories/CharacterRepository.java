package org.tw.battle.domain.repositories;

import org.tw.battle.domain.ServiceConfiguration;
import org.tw.battle.infrastructure.DatabaseConnectionProvider;

import java.sql.*;

import static org.tw.battle.infrastructure.DatabaseConnectionProvider.createConnection;
/**
 * @author Liu Xia
 */
public class CharacterRepository {
    private final ServiceConfiguration configuration;

    public CharacterRepository(ServiceConfiguration configuration) {
        this.configuration = configuration;
    }

    public int create(String name, int hp, int x, int y) throws Exception {
        // TODO:
        //   Please implement the method.
        //   Please refer to DatabaseConnectionProvider to see how to create connection.
        String sql = "INSERT INTO character (name, hp, x, y) VALUES(?, ?, ?, ?)";
        try(Connection connection = createConnection(configuration);
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS))
        {
            statement.setString(1, name);
            statement.setInt(2, hp);
            statement.setInt(3, x);
            statement.setInt(4, y);
            statement.execute();
            final ResultSet keys = statement.getGeneratedKeys();
            keys.next();
            return keys.getInt(1);
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        throw new RuntimeException("Delete this line and implement the method.");
    }

    public String characterInformation (int id) throws Exception {
        String sql =  "SELECT * FROM character WHERE id = " + id;
        Connection connection = createConnection(configuration);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        String message;

        if (resultSet.next()) {
            String name = resultSet.getString("name");
            int hp = resultSet.getInt("hp");
            int x = resultSet.getInt("x");
            int y = resultSet.getInt("y");
            String status  = hp > 0 ? "alive" : "dead";
            message = String.format("id: %d, name: %s, hp: %d, x: %d, y: %d, status: %s", id, name, hp, x, y, status);
        } else {
            message = "character not exist";
        }
        return message;
    }

    public String updateInformation (int id, String name) throws Exception {
        try (Connection newConnection = createConnection(configuration)) {
            if (characterInformation(id).equals("character not exist")) {
                return "character not exist";
            }
            String sql = "UPDATE character SET name = ? WHERE id = ?";
            PreparedStatement newStatement = newConnection.prepareStatement(sql);
            newStatement.setString(1, name);
            newStatement.setInt(2, id);
            newStatement.execute();
            return "Success";
        } catch (Exception e) {
            return "Bad command: rename-character <character id> <new name>";
        }
    }
}
