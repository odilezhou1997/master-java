package org.tw.battle.dbTest;

import org.jooq.*;
import org.jooq.impl.DSL;
import org.tw.battle.domain.ServiceConfiguration;
import org.tw.battle.infrastructure.DatabaseConnectionProvider;

import java.sql.Connection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestQueryHelper {
    static {
        System.setProperty("org.jooq.no-logo", "true");
    }

    public static void assertContainsCharacterTable() throws Exception {
        @SuppressWarnings("unchecked") Result<Record> tables = (Result<Record>) execute(
            (connection, dslContext) -> dslContext.resultQuery("SHOW TABLES").fetch());
        final List<String> names = tables
            .getValues("TABLE_NAME", String.class)
            .stream()
            .map(String::toLowerCase)
            .collect(Collectors.toList());
        assertTrue(names.contains("character"), "Cannot find character table.");

        @SuppressWarnings("unchecked") Result<Record> columns = (Result<Record>) execute(
            (connection, dslContext) -> dslContext.resultQuery("SHOW COLUMNS FROM `character`").fetch());
        final List<String> columnNames = columns.getValues("FIELD", String.class)
            .stream()
            .map(String::toLowerCase)
            .collect(Collectors.toList());
        assertTrue(columnNames.contains("id"), "Cannot find id column in character table.");
        assertTrue(columnNames.contains("name"), "Cannot find name column in character table.");
        assertTrue(columnNames.contains("hp"), "Cannot find hp column in character table.");
        assertTrue(columnNames.contains("x"), "Cannot find x column in character table.");
        assertTrue(columnNames.contains("y"), "Cannot find y column in character table.");
    }

    public static int getCharacterNumber() throws Exception {
        return (int)execute((connection, dslContext) -> dslContext.selectCount()
            .from("character")
            .fetchOne(0, int.class));
    }

    public static Record getCharacter(int id) throws Exception {
        return (Record)execute((connection, dslContext) -> dslContext.select(
            field("id"),
            field("name"),
            field("hp"),
            field("x"),
            field("y"))
            .from(table("character"))
            .where(field("id").eq(id))
            .fetchOne());
    }

    public static String getCharacterName(int id) throws Exception {
        final Record record = (Record) execute((connection, dslContext) -> dslContext.select(field("name"))
            .from(table("character"))
            .where(field("id").eq(id))
            .fetchOne());
        return record.get("name", String.class);
    }

    private static Object execute(BiFunction<Connection, DSLContext, Object> func) throws Exception {
        final ServiceConfiguration configuration = TestDatabaseConfiguration.getConfiguration();
        try (
            final Connection connection = DatabaseConnectionProvider.createConnection(configuration);
            final DSLContext context = DSL.using(connection, SQLDialect.H2)
        ) {
            return func.apply(connection, context);
        }
    }
}
