package org.tw.battle.mocks;

import org.tw.battle.domain.CommandHandler;
import org.tw.battle.domain.CommandResponse;

public class CommandHandlerWithUnhandledException implements CommandHandler {
    private final String message;

    public CommandHandlerWithUnhandledException(String message) {
        this.message = message;
    }

    @Override
    public boolean canHandle(String commandName) {
        return true;
    }

    @Override
    public CommandResponse handle(String[] commandArgs) {
        throw new RuntimeException(message);
    }
}
